Example usage with example result:

```
vendor/bin/phpstan analyse -v services.php --level 5
 1/1 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100% < 1 sec

 ------ ----------------------------------------------------------------------------
  Line   services.php
 ------ ----------------------------------------------------------------------------
  5      Class App\Klasa constructor invoked with 1 parameter, 3 required.
  5      Parameter #1 $a of class App\Klasa constructor expects bool, string given.
 ------ ----------------------------------------------------------------------------


 [ERROR] Found 2 errors


Used memory: 46 MB
```
